package lab6B.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;

import java.util.List;

//corespunde paginii care ii arata utilizatorului contul sau
public class AccountPage extends PageObject {

    //vom interactiona cu butonul de logout si vom verifica daca am ajuns in aceasta pagina

    //butonul de logout il identificam dupa xpath deoarece nu am gasit pentru el un mijloc mai simplu
    @FindBy(xpath = "//*[@id=\"StatusbarForm\"]/a[3]/img")
    private WebElementFacade logoutButton;

    //butonul de create directory il identificam dupa xpath
    @FindBy(xpath = "//*[@id=\"smallbutton\"]")
    private WebElementFacade newDirButton;

    //butonul de delete directory il identificam dupa full xpath, nu merge dupa value = "Delete"
    @FindBy(xpath = "/html/body/div/div[2]/div/form/table[2]/tbody/tr[1]/td/table/tbody/tr/td[2]/input[3]")
    private WebElementFacade deleteDirButton;

    //pe butonul de delete directory vom da click
    public void click_delDir() {
        deleteDirButton.click();
    }

    //am mai găsit 2 elemente mai usor de identificat care contin numele utilizatorului, de exemplu, TextBox-ul care contine numele directorului in care ne aflam, situat in artea e stanga sus a paginii
    //nu putem sa ne folosim de nume in identificarea sa, deoarece nu e unicul element cu numele "directory", de aceea folosim si pentru acesta xpath
    @FindBy(xpath = "//*[@id=\"toptable\"]/tbody/tr/td[2]/input")
    private WebElementFacade currentDir;

    //pe butonul de logout vom da click
    public void click_logout() {
        logoutButton.click();
    }

    //pe butonul de create directory vom da click
    public void click_newDir() {
        newDirButton.click();
    }

    //vom scoare valoarea directorului curent, pentru a verifica in steps daca ea se termina cu numele utilizatorului
    public String get_currentDir() {
        return currentDir.getValue();
    }

//    codul pentru selectarea unui director din listă (în vederea ștergerii)
//    funcția "click_element_by_attribute_value" poate fi folosită (cu parametrii corespunzători) și pentru acționarea butonului ”Delete”
    @FindBy(id = "maintable")
    private WebElementFacade listOfDirectories;
    private void click_element_by_attribute_value(WebElementFacade el, String attribute, String value) {
        List<WebElement> list = el.findElements(By.tagName("input"));
        for (int i = 0; i<list.size(); i++){
            String dir = list.get(i).getAttribute(attribute);
            if (dir.equalsIgnoreCase(value)) {
                list.get(i).click();
                break;
            }
        }
    }
    public void check_directory_to_delete(String directory) {
        click_element_by_attribute_value(listOfDirectories, "value", directory);
    }
}