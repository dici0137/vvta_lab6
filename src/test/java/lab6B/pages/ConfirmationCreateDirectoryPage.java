package lab6B.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

//corespunde paginii care ii arata utilizatorului pagina sa creeam noi directoare
public class ConfirmationCreateDirectoryPage extends PageObject {

    //vom interactiona cu numele generat  si vom verifica daca am ajuns in aceasta pagina

    //numele generat il identificam dupa xpath deoarece nu am gasit pentru el un mijloc mai simplu
    @FindBy(xpath = "//*[@id=\"NewDirForm\"]/b")
    private WebElementFacade newNameDirConfirmetionText;

    //butonul de back il identificam dupa xpath
    @FindBy(xpath = "//*[@id=\"NewDirForm\"]/a/img")
    private WebElementFacade BackButton;

    //pe back vom da click
    public void click_BackButton() {
        BackButton.click();
    }

    //vom scoare valoarea directorului curent, pentru a verifica in steps daca ea se termina cu numele utilizatorului
    public String get_DirName() {
        return newNameDirConfirmetionText.getValue();
    }

    public String get_textDirName() {
        //se ia lista de div-uri de pe pagina
        WebElementFacade DirName = find(By.tagName("b"));
        //intr-unul din div-uri se afla o lista de h1 in care se afla cuvantul cautat de noi
        return DirName.getValue();
    }

    public List<String> get_textCrtDirMessage() {
        //se ia lista de div-uri de pe pagina
        WebElementFacade bodyList = find(By.tagName("div"));
        //intr-unul din div-uri se afla o lista de h1 in care se afla mesajul " was successfully created." cautat de noi
        return bodyList.findElements(By.tagName("form")).stream()
                .map( element -> element.getText() )
                .collect(Collectors.toList());
    }
}