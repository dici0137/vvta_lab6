package lab6B.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;

import java.util.List;

//corespunde paginii care ii arata utilizatorului pagina sa stergem noi directoare
public class DeleteDirectoryPage extends PageObject {

    //vom interactiona cu  tabelul care contine lista de directoare existente si butoane inclusiv cel pentru setrgere/delete





    //butonul de ok/Submit Delete directory il identificam dupa xpath
    @FindBy(xpath = "//*[@id=\"CopyMoveDeleteForm\"]/a[2]/img")
    private WebElementFacade SubmitDelDirButton;

    //pe butonul de Submit Delete  directory vom da click
    public void click_SubmitDelDir() {  SubmitDelDirButton.click();    }

}