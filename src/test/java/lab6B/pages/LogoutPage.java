package lab6B.pages;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

//aceasta functie permite extragerea pe baza unor tag-uri a parti din continutul unei pagini web
//dorim sa ajungem la un text care contine "logged", de la "v-ati delogat cu succes", care se afla intr-o lista de div-uri, respectiv o sublista de h1
public class LogoutPage extends PageObject {

    public List<String> get_textLogout() {
        //se ia lista de div-uri de pe pagina
        WebElementFacade bodyList = find(By.tagName("div"));
        //intr-unul din div-uri se afla o lista de h1 in care se afla cuvantul cautat de noi
        return bodyList.findElements(By.tagName("p")).stream()
                .map( element -> element.getText() )
                .collect(Collectors.toList());
    }
}