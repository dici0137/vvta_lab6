package lab6B.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

//corespunde paginii care ii arata utilizatorului pagina sa creeam noi directoare
public class ConfirmationDeleteDirectoryPage extends PageObject {

    //
    //butonul de back il identificam dupa xpath
    @FindBy(xpath ="//*[@id=\"CopyMoveDeleteForm\"]/a/img")
    private WebElementFacade BackButton;

    //pe back vom da click
    public void click_BackButton() {
        BackButton.click();
    }

    public List<String> get_textDelMessage() {
        //se ia lista de div-uri de pe pagina
        WebElementFacade bodyList = find(By.tagName("div"));
        //intr-unul din div-uri se afla o lista de h1 in care se afla mesajul cautat de noi  / "All the selected directories and files have been processed."
        return bodyList.findElements(By.tagName("form")).stream()
                .map( element -> element.getText() )
                .collect(Collectors.toList());
    }
}