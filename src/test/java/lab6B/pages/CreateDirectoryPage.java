package lab6B.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

//corespunde paginii care ii arata utilizatorului pagina sa creeam noi directoare
public class CreateDirectoryPage extends PageObject {

    //vom interactiona cu primul textbox  insert create directory si vom verifica daca am ajuns in aceasta pagina

    //primul texbox insert create directory il identificam dupa xpath deoarece nu am gasit pentru el un mijloc mai simplu
    @FindBy(name = "newNames[1]")
    private WebElementFacade newNameInsertBox;

    //in textbox-ul cu new directory name  vom scrie numele noilui director
    public void enter_newDirectory(String name) {
        newNameInsertBox.type(name);
    }

    //butonul de ok/Submit new directory il identificam dupa xpath
    @FindBy(xpath = "//*[@id=\"NewDirForm\"]/a[2]/img")
    private WebElementFacade SubmitNewDirButton;

    //pe butonul de create directory vom da click
    public void click_SubmitNewDir() {  SubmitNewDirButton.click();    }
}