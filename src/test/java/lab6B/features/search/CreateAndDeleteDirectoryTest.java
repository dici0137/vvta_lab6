package lab6B.features.search;
import lab6B.steps.serenity.*;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

//acest fisier se executa
@RunWith(SerenityRunner.class)
public class CreateAndDeleteDirectoryTest {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    //vom folosi pasii corespunzatori paginii de login si account
    @Steps
    public LoginSteps login;

    @Steps
    public AccountSteps account;

    @Steps
    public LogoutSteps logout;

    @Steps
    public CreateDirectorySteps Crdir;

    @Steps
    public ConfirmCreateDirectorySteps ConCrDir;

    @Steps
    public DeleteDirectorySteps DelDir;

    @Steps
    public ConfirmDeleteDirectorySteps ConDelDir;

    @Issue("#login-1")
    @Test
    public void createdirectorytest() {
        //pornim de la pagina de login
        login.is_the_home_page();
        //se introduc toate datele si se da click pe butonul de Login
        login.enters_data_and_click_login("linux.scs.ubbcluj.ro","vvta","2019vvta");
        //verificam ca am ajuns in contul utilizatorului vvta
        account.should_see_username("vvta");

        account.click_newDir();

        Crdir.completareSiClick("DirNou");

        ConCrDir.verificareSiClick(" was successfully created.");

        account.selectDirSiClickDelete("DirNou");

        DelDir.clickSubmitDeleteDir();

       ConDelDir.verificareDelSiClick("All the selected directories and files have been processed.");

        //se iese din cont
        account.click_logout();
        logout.should_see_text("logged out");

    }
}