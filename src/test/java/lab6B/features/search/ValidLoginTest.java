package lab6B.features.search;
import lab6B.pages.AccountPage;
import lab6B.steps.serenity.*;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

//acest fisier se executa
@RunWith(SerenityRunner.class)
public class ValidLoginTest {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    //vom folosi pasii corespunzatori paginii de login si account
    @Steps
    public LoginSteps login;

    @Steps
    public AccountSteps account;

    @Steps
    public ConfirmCreateDirectorySteps create;

    @Steps
    public ConfirmDeleteDirectorySteps delete;

    @Steps
    public LogoutSteps logout;


    @Issue("#login-1")
    @Test
    public void validLogintest() {
        //pornim de la pagina de login
        login.is_the_home_page();
        //se introduc toate datele si se da click pe butonul de Login
        login.enters_data_and_click_login("linux.scs.ubbcluj.ro","dici0137","7Ja58b5f5J");
        //verificam ca am ajuns in contul utilizatorului dici0137
        account.should_see_username("dici0137");
//        create.verificareSiClick(" was successfully created.");
//        delete.verificareDelSiClick("All the selected directories and files have been processed.");
        //se iese din cont
        account.click_logout();
        logout.should_see_text("logged out");
    }

} 