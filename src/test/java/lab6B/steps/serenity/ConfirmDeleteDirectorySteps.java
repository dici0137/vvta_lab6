package lab6B.steps.serenity;

import net.thucydides.core.annotations.Step;
import lab6B.pages.ConfirmationDeleteDirectoryPage;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertThat;

//in aceasta clasa definim pasii de urmat in pagina contului utilizatorului
public class ConfirmDeleteDirectorySteps {

    //avem nevoie de pagina de stergere cu confirmarea
    ConfirmationDeleteDirectoryPage ConfirmDeleteDirPage;

    //in acest pas vom verifica daca se afla textul "All the selected directories and files have been processed." in locul identificat de pe pagina
    //un step
    @Step
    public void verificareDelSiClick(String text) {

        assertThat(ConfirmDeleteDirPage.get_textDelMessage(), hasItem(containsString(text)));

        ConfirmDeleteDirPage.click_BackButton();
       }
}