package lab6B.steps.serenity;

import net.thucydides.core.annotations.Step;
import lab6B.pages.*;

import static org.junit.Assert.assertTrue;

//in aceasta clasa definim pasii de urmat in pagina contului utilizatorului
public class CreateDirectorySteps {

    //avem nevoie de pagina utilizatorului
    CreateDirectoryPage CreateDirPage;
    //un step pentru completare si Click pe buton submit
    @Step
    public void completareSiClick(String name) {
        CreateDirPage.enter_newDirectory(name);
        CreateDirPage.click_SubmitNewDir();
    }

}