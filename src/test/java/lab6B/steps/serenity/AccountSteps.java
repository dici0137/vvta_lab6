package lab6B.steps.serenity;
import lab6B.pages.AccountPage;
import net.thucydides.core.annotations.Step;
import static org.junit.Assert.assertTrue;

//in aceasta clasa definim pasii de urmat in pagina contului utilizatorului
public class AccountSteps {

    //avem nevoie de pagina utilizatorului
    AccountPage accountPage;

    //un pas ar fi verificarea daca directorul curent se termina cu numele utilizatorului
    @Step
    public void should_see_username(String username) {
        //am folosit urmatoarea linie in debug pentru a vedea ce valoare are Directorul curent
        String s=accountPage.get_currentDir();
        assertTrue(accountPage.get_currentDir().contains(username));
    }

    //un alt pas e cel in care utilizatorul se delogheaza
    @Step
    public void click_logout() {
        accountPage.click_logout();
    }

   @Step
   public void click_newDir() {       accountPage.click_newDir();   }

    @Step
    public void selectDirSiClickDelete(String name) {

        accountPage.check_directory_to_delete(name);
        //DeleteDirPage.enter_newDirectory(name);
        accountPage.click_delDir();

    }

}