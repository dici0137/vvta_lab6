package lab6B.steps.serenity;

import lab6B.pages.LogoutPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertThat;

//in aceasta clasa definim pasii de urmat in pagina ce apare dupa o delogare cu succes
public class LogoutSteps {

    //lucram cu pagina de Logout
    LogoutPage logoutPage;

    //in acest pas vom verifica daca se afla cuvantul "logged" in locul identificat de pe pagina
    @Step
    public void should_see_text(String text) {
        assertThat(logoutPage.get_textLogout(), hasItem(containsString(text)));
    }

}