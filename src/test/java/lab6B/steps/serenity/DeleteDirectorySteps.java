package lab6B.steps.serenity;

import net.thucydides.core.annotations.Step;
import lab6B.pages.*;

//in aceasta clasa definim pasii de urmat in pagina contului utilizatorului
public class DeleteDirectorySteps {

    //avem nevoie de pagina utilizatorului
    DeleteDirectoryPage DeleteDirPage;
    //un step pentru completare si Click pe buton submit

    @Step
    public void clickSubmitDeleteDir() {
        DeleteDirPage.click_SubmitDelDir();
    }

}