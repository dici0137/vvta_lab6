package lab6B.steps.serenity;

import net.thucydides.core.annotations.Step;
import lab6B.pages.*;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.*;

//in aceasta clasa definim pasii de urmat in pagina contului utilizatorului
public class ConfirmCreateDirectorySteps {

    //avem nevoie de pagina utilizatorului
    ConfirmationCreateDirectoryPage ConfirmCreateDirPage;

    //un step
    //in acest pas vom verifica daca se afla textul " was successfully created." in locul identificat de pe pagina
    @Step
    public void verificareSiClick(String text) {
        assertThat(ConfirmCreateDirPage.get_textCrtDirMessage(), hasItem(containsString(text)));
        ConfirmCreateDirPage.click_BackButton();
       }
}