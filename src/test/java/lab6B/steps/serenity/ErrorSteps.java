package lab6B.steps.serenity;

import lab6B.pages.ErrorPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertThat;


//in aceasta clasa definim pasii de urmat in pagina de raportare a erorii
public class ErrorSteps {

    //avem nevoie de pagina de raportare a erorii
    ErrorPage errorPage;

    //in acest pas vom verifica daca se afla testul "error" in locul identificat de pe pagina
    @Step
    public void should_see_text(String text) {
        assertThat(errorPage.get_textError(), hasItem(containsString(text)));
    }

}